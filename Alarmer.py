#!/usr/bin/python

import smtplib

sender = 'doyeong.kim@anl.gov'
receivers = ['doyeong.kim@anl.gov'] #,
#'cundiff@anl.gov', 'twamorkar@anl.gov', 'mjadhav@anl.gov', 'jmetcalfe@anl.gov']

# https://edms.cern.ch/ui/#!master/navigator/document?P:1563561654:101196507:subDocs
def messages(alarmtype, currentdata):
    if alarmtype=="test":
        msg = "This is just for test. Please, let me know if you received this email, and if you want to receive AMAF storage alarm email in the future. The email will be sent automatically when the storage condition is out of spec."

    if alarmtype=="temperaturebad":
        msg = "Temperature is out of range!! \n%s\nYou'll receive another email when it is back to normal(temperature range of 18C to 24C)."%(currentdata)

    if alarmtype=="temperaturegood":
        msg = "Temperature is back to normal :) \n%s\n(temperature range of 18C to 24C)."%(currentdata)

    if alarmtype=="humiditybad":
        msg = "Humidity is out of range!! \n%s\nYou'll receive another email when it is back to normal(RH between 0% and 30%)."%(currentdata)

    if alarmtype=="humiditygood":
        msg = "Humidity is back to normal!! \n%s\n(RH between 0% and 30%)."%(currentdata)

    if alarmtype=="datastreambad":
        msg = "There is something wrong in our datastream!! \n%s\nYou'll receive another email once our datastream is back to normal."%(currentdata)

    if alarmtype=="datastreamgood":
        msg = "Datastream is back to normal!! \n%s"%(currentdata)

    return msg

def send(alarmtype, currentdata):

    message = """From: ANL AMAF Storage Alarmer <amaf.storage@alarmer>
To: To The AMAF Avengers <amaf@amaf.com>
Subject: AMAF Lab Infrastructure - Storage Alarmer!

Dear AMAF Avengers,

{msg}

Best regards,
Doyeong

""".format(msg = messages(alarmtype, currentdata))

    smtpObj = smtplib.SMTP('localhost')
    smtpObj.sendmail(sender, receivers, message)
    print ("Successfully sent email")
    print (message)


