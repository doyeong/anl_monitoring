#!/usr/bin/env python

from influxdb import InfluxDBClient
import datetime
import argparse, serial, time
import os, sys, re
import subprocess
import math
import Alarmer as ala

filename = time.strftime("CanaryBoard_data_Lab_%Y%m%d%H%M%S.txt")
tmp_file=open(filename, "a+")
tmp_file.write(f"Time\tRoomTemp\tHumidity\tDewPoint\tParticleCount\n")
tmp_file.close()

TempStatus, HumidStatus, DatastreamStatus = "good", "good", "good"

def readSerial(args):
    """Read the serial output and print it to screen."""

    #------------------------------------------
    #serial setup
    ser = serial.Serial(
        port = args.port,
        baudrate = args.baudrate,
        parity = serial.PARITY_NONE,
        stopbits = serial.STOPBITS_ONE,
        bytesize = serial.EIGHTBITS,
        timeout = 60)
    ser.flushInput()

    #------------------------------------------

    #loop
    while True:
        try:
            string = ser.readline().decode("utf-8", errors="ignore").strip("\n")
            print(time.strftime("%Y%m%d%H%M%S"), string)
            tmp_file=open(filename, "a+")
            tmp_file.write("{}\t{}".format(time.strftime("%Y%m%d%H%M%S"),string))
            tmp_file.close()
            return string
        except KeyboardInterrupt:
            print()
            sys.exit(0)

def read_data(string):

    temperature = '-999' if string.split()[0]=='nan' else string.split()[0]
    humidity = '-999' if string.split()[1]=='nan' else string.split()[1]

    if string.split()[2]=='nan' and float(humidity)==0.0:
        H = (math.log(float(humidity)+0.5, 10)-2)/0.4343 + (17.62*float(temperature))/(243.12+float(temperature))
        dewpoint = 243.12*H/(17.62-H)
    elif float(humidity)!=0.0:
        H = (math.log(float(humidity), 10)-2)/0.4343 + (17.62*float(temperature))/(243.12+float(temperature))
        dewpoint = 243.12*H/(17.62-H)
    else:
        dewpoint = -999.
    print("corrected dewpoint: %s"%dewpoint)
    #dewpoint = '-999' if string.split()[2]=='nan' else string.split()[2]

    particlecnt = '-999' if string.split()[3]=='nan' else string.split()[3]
    #print (temperature, humidity, dewpoint, particlecnt)
    data_list = [{
        'measurement' : 'ANL-AMAF-LabInfrastructureL-Lab',
        'tags':{'cpu': 'cleanroom'},
        'fields':{
            'time': datetime.datetime.now().strftime("%H:%M:%S"),
            'temperature': float(temperature),
            'humidity': float(humidity),
            'dewpoint': dewpoint,
            'particlecnt': float(particlecnt)
        }
    }]

    # Call alarmer
    global TempStatus
    global HumidStatus
    currentdata = "time: %s\ntemp: %s\nhumid: %s\ndewpnt: %s\nparticlecnt: %s"%(datetime.datetime.now().strftime("%H:%M:%S"), temperature, humidity, str(dewpoint), particlecnt)
    if TempStatus == "good" and (float(temperature)<16 or float(temperature)>24):
        TempStatus = "bad"
        ala.send("temperaturebad", currentdata)        
    if TempStatus == "bad" and (float(temperature)>16 and float(temperature)<24):
        TempStatus = "good"
        ala.send("temperaturegood", currentdata)        
    if HumidStatus == "good" and (float(humidity)<15 or float(humidity)>60):
        HumidStatus = "bad"
        ala.send("humiditybad", currentdata)        
    if HumidStatus == "bad" and (float(humidity)>15 and float(humidity)<60):
        HumidStatus = "good"
        ala.send("humiditygood", currentdata)        

    return data_list
    
def main():
    #------------------------------------------
    #parse input arguments
    parser = argparse.ArgumentParser(description="%prog [options]")
    parser.add_argument("-p", "--port", dest="port", default="/dev/ttyUSB1", help="serial port, usually\"/dev/cu.SLAB_USBtoUART\" or \"/dev/ttyUSB1\"")
    parser.add_argument("-b", "--baudrate", dest="baudrate", default=115200, help="baud rate")
    args = parser.parse_args()

    #------------------------------------------

    print("START")

    # InfluxDB client
    client = InfluxDBClient(host='localhost',port=8086)
    #client.create_database('LabInfrastructureMonitoringLabDB')
    client.switch_database('LabInfrastructureMonitoringDB')

    while True:
        # Write data
        client.write_points(read_data(readSerial(args)))

if __name__ == "__main__":
    main()
